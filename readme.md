# HTML5 Gulp Starterkit

This is my front-end framework for html/ css/ js development using NPM, Bower, Gulp, Sass (with Autoprefixer), SourceMaps, Browser-Sync, Imagemin, Uglify, Concat, html-minify and inline-minify. 

## Install Packages

After cloning the project run the following command in your terminal to install all required node and bower packages.

	1.  npm install && bower install	
		

## Start

To start building simply add the site to the `app` folder being sure to use the following file locations for css and js.

```
/assets/css/style.css
/assets/js/app.min.js
```

Then you can run the automated compiling with:

	gulp
	
	or
	
	gulp php

## Build

Create a deployment build with the following commands:

	gulp build
	
You can now copy the contents of the `htdocs` folder to the server.

## Test App Build

To fire up a server and test the final build:

	gulp build:serve
	
	or
	
	gulp build:serve-php

---------------------------------------

## gulpfile.js
Javascript concatenation is done in the config object in the guilpfile.  This controls the order as well as files to be be concatenated. The config object also controls which files are EXCLUDED from the final build.

## .bowerrc
Controls the location where bower packages will be installed.

## File Structure

src is where you develop the sass and js.

Starter boilerplate html files are in `src/defaults` folder:
- 404.html
- apple-touch-icon.png
- favicon.ico
- index.html
- style.html

---

###Adding modules to the project

	npm install --save-dev module-name

	bower install --save normalize.css

Including `--save-dev` for npm and `--save` for bower adds it to the project as a dependency!

---

This started as a fork of JLGulp by [Joel Longie](https://github.com/joellongie/jlgulp) which is accompanied by an excellent tutorial which you can find here on [Youtube](https://www.youtube.com/watch?v=LmdT2zhFmn4).
 
Detailed info can also be found on his [blog](http://joellongie.com/gulp-build-system-fundamentals/).

---

Todos

[] Refine watch tasks.  
[] Build default Sass structure  
[] Maybe move the bower folder..?  

---
Changelog
v0.1.2 2015-11-01
- Added 'gulp-connect-php' to package.json
- Add `connectPHP = require('gulp-connect-php');` to vars
- Remove .php .txt and .md watch tasks
- Add .css .js .php .txt and .md tasks to broswer-sync-php task
