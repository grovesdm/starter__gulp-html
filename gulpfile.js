// ////////////////////////////////////////////////
//
// EDIT CONFIG OBJECT BELOW !!!
//
// jsConcatFiles => list of javascript files (in order) to concatenate
// buildFilesFoldersRemove => list of files to remove when running final build
// // /////////////////////////////////////////////

var config = {
    jsConcatFiles: [
        './src/js/module1.js',
        './src/js/main.js'
    ],
    buildFilesFoldersRemove:[
        'htdocs/assets/js/!(*.min.js)',
        'htdocs/assets/maps/'
    ]
};


// ////////////////////////////////////////////////
// Required taskes
// gulp build
// bulp build:serve
// // /////////////////////////////////////////////

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    minifyHTML = require('gulp-minify-html'),
    minifyInline = require('gulp-minify-inline'),
    connectPHP = require('gulp-connect-php');


// ////////////////////////////////////////////////
// Log Errors
// // /////////////////////////////////////////////

function errorlog(err){
    console.error(err.message);
    this.emit('end');
}


// ////////////////////////////////////////////////
// Scripts Tasks
// ///////////////////////////////////////////////

gulp.task('scripts', function() {
    return gulp.src(config.jsConcatFiles)
        .pipe(sourcemaps.init())
        .pipe(concat('temp.js'))
        .pipe(uglify())
        .on('error', errorlog)
        .pipe(rename('app.min.js'))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('./app/assets/js/'))

        .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// Styles Tasks
// ///////////////////////////////////////////////

gulp.task('styles', function() {
    gulp.src('src/scss/style.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}))
        .on('error', errorlog)
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.write('../maps'))
        .pipe(gulp.dest('app/assets/css'))
        .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// HTML Tasks
// // /////////////////////////////////////////////

gulp.task('html', function(){
    gulp.src('app/**/*.html')
        .pipe(reload({stream:true}));
});


// ////////////////////////////////////////////////
// Browser-Sync Tasks
// // /////////////////////////////////////////////

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./app/"
        }
    });
});

gulp.task('browser-sync-php', ['php-server'], function () {
    browserSync.init(['./app/**/**.php', './app/**/**.txt', './app/**/**.md'], {
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
});

gulp.task('php-server', function() {
    connectPHP.server({
        hostname: '127.0.0.1',
        bin: 'C:/php/php.exe',
        ini: 'C:/php/php.ini',
        port: 8010,
        base: './app/'
    });
});


// task to run build server for testing final app
gulp.task('build:serve', function() {
    browserSync({
        server: {
            baseDir: "./htdocs/"
        }
    });
});

gulp.task('build:serve-php', ['php-server-build'], function () {
    browserSync({
        proxy: '127.0.0.1:8010',
        port: 8080,
        open: true,
        notify: false
    });
});

gulp.task('php-server-build', function() {
    connectPHP.server({
        hostname: '127.0.0.1',
        bin: 'C:/php/php.exe',
        ini: 'C:/php/php.ini',
        port: 8010,
        base: './htdocs/'
    });
});

// ////////////////////////////////////////////////
// Build Tasks
// // /////////////////////////////////////////////

// clean out all files and folders from build folder
gulp.task('build:cleanfolder', function (cb) {
    del([
        'htdocs/**'
    ], cb);
});

// task to create build directory of all files
gulp.task('build:copy', ['build:cleanfolder'], function(){
    return gulp.src('app/**/*/')
        .pipe(gulp.dest('htdocs/'));
});

// Compress Images
gulp.task('img', ['build:copy'], function () {
    return gulp.src('./app/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('htdocs/'));
});

gulp.task('minify-html', ['build:copy'], function() {
    var opts = {
        conditionals: true,
        spare:true
    };

    return gulp.src('app/**/*.html')
        .pipe(minifyHTML(opts))
        .pipe(gulp.dest('./htdocs/'));
});

gulp.task('minify-inline', ['minify-html'], function() {
    gulp.src('htdocs/*.html')
        .pipe(minifyInline())
        .pipe(gulp.dest('htdocs/'))
});
// task to removed unwanted build files
// list all files and directories here that you don't want included
gulp.task('build:remove', ['build:copy', 'img', 'minify-html', 'minify-inline'], function (cb) {
    del(config.buildFilesFoldersRemove, cb);
});

gulp.task('build', ['build:copy', 'img', 'minify-html', 'minify-inline', 'build:remove']);



// ////////////////////////////////////////////////
// Watch Tasks
// // /////////////////////////////////////////////

gulp.task ('watch', function(){
    gulp.watch('src/scss/**/*.scss', ['styles']);
    gulp.watch('src/js/**/*.js', ['scripts']);
    gulp.watch('app/**/*.html', ['html']);
});


gulp.task('default', ['scripts', 'styles', 'html', 'browser-sync', 'watch']);

gulp.task('php', ['scripts', 'styles', 'html', 'browser-sync-php', 'watch']);